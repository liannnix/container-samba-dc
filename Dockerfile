FROM alt:p10

LABEL maintainer="sin@basealt.ru; kaa@basealt.ru"

ADD ./bash.sh /

WORKDIR /

ARG PACKAGE
ARG DNS_PORT='53 53/udp'

RUN if [[ -n "$PACKAGE" ]] && [[ "$PACKAGE" =~ ^(MIT|mit|mitkrb5|task-samba-dc-mitkrb5)$ ]]; then PACKAGE='task-samba-dc-mitkrb5'; \
         else PACKAGE='task-samba-dc' ; fi && \
    apt-get update && \
    apt-get install "$PACKAGE" -y && \
    apt-get dist-upgrade -y && \
    #Cleaning image \
    apt-get clean && \
    mkdir -p /var/lock/subsys && chmod 700 /var/lock/subsys && \
    find /var/lib/apt -type f -not -name 'lock' -not -path '*/partial' -exec rm -rf  {} +  && \
    find /var/cache/apt -type f -not -name 'lock' -not -path '*/partial' -exec rm -rf  {} +

EXPOSE 88 88/udp 135 137-138/udp 139 389 389/udp 445 464 464/udp 636 3268-3269 49152-65535
EXPOSE $DNS_PORT

ENTRYPOINT [ "/bin/bash", "-l", "-c" ]
CMD [ "bash /bash.sh" ]