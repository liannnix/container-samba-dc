#!/bin/bash

#in dev

while [ -n "$1" ]
  do
  case "$1" in
    --krb-auth-type=*)
      KRB_AUTH_TYPE="${1#*=}"
      echo -n "Kerberos authentication type: $KRB_AUTH_TYPE; "
      shift;;
    --last-version=*)
      LAST_VERSION="${1#*=}"
      echo -n "Version for the image has been set like: $LAST_VERSION;"
      shift;;
  esac
done

docker build --build-arg PACKAGE="$KRB_AUTH_TYPE" -t '192.168.200.2:5000/basealt/samba-core-dc-mitkrb5:latest' -t "192.168.200.2:5000/basealt/samba-core-dc-mitkrb5:$LAST_VERSION" .