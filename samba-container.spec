%def_enable man

Name: libshell
Version: 0.0.1
Release: alt1

Summary: Samba core in docker container
#License: GPL-2.0-or-later
#Group: Development/Other
BuildArch: amd64
Packager: Aleksei Kalinin <kaa@altlinux.ru>

Url: https://gitlab.com/faktor.lex/samba_docker.git

Source: %name-%version.tar

BuildRequires: docker-engine
BuildRequires: samba-common
BuildRequires: libshell


%description
This package contains utilities and alternatives to migrate samba DC to docker container

%build

%check

%files
/bin/*
%exclude /bin/
%_datadir/%name
%doc COPYING
%if_enabled man
%_man3dir/*


%changelog
